/*
 * musician.h
 *
 *  Created on: 5 Aug 2016
 *      Author: User
 *
 * Describes the guy who is playing music. This guy plays his
 * music using PWM output.
 *
 * Left channel: PTC1 (FTM0_CH0)
 * Right channel: PTC2 (FTM0_CH1)
 *
 * How to connect:
 *
 * 		PWM channel output							Audio
 * 			---------------------- 1k Ohm ------------
 * 							|
 * 						   4.7nF
 * 							|
 * 						   GND
 */

#ifndef MUSICIAN_H_
#define MUSICIAN_H_

#include "fsl_ftm.h"
#include "defines.h"

result musician_init();

result musician_play(uint16_t, uint16_t);

#endif /* MUSICIAN_H_ */

/*
 * conductor.h
 *
 *  Created on: 4 Aug 2016
 *      Author: User
 *
 * This file describes the music conductor who reads from his sheet
 * music and who directs musicians. Every x ms (ie. at 44.1kHz ideally),
 * he will tell the musicians what to play.
 *
 * Implements the tempo as a interrupt timer (Periodic Interrupt Time, PIT).
 */

#ifndef CONDUCTOR_H_
#define CONDUCTOR_H_

#include "fsl_gpio.h"
#include "fsl_pit.h"
#include "defines.h"

#include "sheet.h"
#include "musician.h"

volatile bool next_tempo;

void conductor_init();

void conductor_play(char *);


#endif /* CONDUCTOR_H_ */

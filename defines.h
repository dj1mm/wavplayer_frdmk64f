/*
 * sheet.h
 *
 *  Created on: 3 Aug 2016
 *      Author: User
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include "fsl_debug_console.h"
#include "board.h"

#define BUFFER_SIZE 256
#if (((BUFFER_SIZE - 1) & BUFFER_SIZE) != 0)
#error BUFFER SIZE should be a power of 2 because it is used by the ring buffer.
#endif

#ifdef SDK_DEBUGCONSOLE

#define printf PRINTF

#endif

typedef enum {
	Ok = 0, 				// NO error

	ExternalError,			// External errors like no sdcard
	RuntimeError, 			// Internal error
	InvalidArgument,		// Invalid argument cause error
	InvalidProcessedData,	// Invalid data when working in the function cause error
} result;

#endif /* SHEET_H_ */

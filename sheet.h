/*
 * sheet.h
 *
 *  Created on: 3 Aug 2016
 *      Author: User
 *
 *  Based on the sdcard example for KSDK v2.
 *  Basically implements a 'sheet music'. Will read what to play
 *  from a sd card, and save it to a ring buffer. Will tell the conductor
 *  what to play when he asks.
 *
 *  Uses the sd card slot available on the k64f
 */

#ifndef SHEET_H_
#define SHEET_H_

#include "fsl_gpio.h"
#include "defines.h"

#include "ff.h"
#include "diskio.h"

// BIG and LITTLE Endians
// For eg:
// Consider the number 16000. The MSB is on the left such that 16000 = 0x00003E80
// the number 16000 = 00 00 3E 80 in big endian
// the number 16000 = 80 3E 00 00 in little endian

inline uint32_t parse4ByteLittleEndian(uint8_t *addr) {
	return ((uint32_t) *addr << 24) | ((uint32_t) *(addr+1) << 16) | ((uint32_t) *(addr+2) << 8) | ((uint32_t) *(addr+3));
}

inline uint32_t parse4ByteBigEndian(uint8_t *addr) {
	return ((uint32_t) *(addr+3) << 24) | ((uint32_t) *(addr+2) << 16) | ((uint32_t) *(addr+1) << 8) | ((uint32_t) *addr);
}

inline uint16_t parse2ByteLittleEndian(uint8_t *addr) {
	return ((uint16_t) *addr << 8) | ((uint16_t) *(addr+1));
}

inline uint16_t parse2ByteBigEndian(uint8_t *addr) {
	return ((uint16_t) *(addr+1) << 8) | ((uint16_t) *addr);
}

typedef struct {
	uint16_t numChannels;
	uint32_t sampleRate;
	uint16_t bitsPerSample;
	uint32_t size;
} format;

/**
 * Init sheet dependents (the sd card and the ring buffer).
 * To be called oni once.
 */
result sheet_init();

/**
 * Cleans the buffer/open the score haha
 */
result sheet_start();

/**
 * Quickly scan the score and see whether it is playable or
 * not.
 * Returns Ok if successful,
 * Returns something else if unsuccessful
 * 		ExternalError: Smething external (no sdcard plugged in)
 * 		InvalidArgument: The file we want to open doesnot exist/is invalid
 * 		RuntimeError:
 * 		InvalidProcessedData: The data in the file is not correct.
 */
result sheet_sightSee(char *, format *);

/**
 * Check whether we finished the sheet. true if yes.
 */
bool sheet_end();

/**
 * Skim the score and save it in a buffer to be played later.
 * Will not do anything if the buffer is currently full.
 */
result sheet_skim(format *);

/**
 * Returns what is to play next. Will just return 0 if the
 * buffer is currently empty.
 */
uint32_t sheet_read();

/**
 * Basically closes the sheet music after having played the music
 */
result sheet_finish();

#endif /* SHEET_H_ */

/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "fsl_device_registers.h"
#include "fsl_port.h"
#include "pin_mux.h"

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Initialize all pins used in this example
 *
 * @param disablePortClockAfterInit disable port clock after pin
 * initialization or not.
 */
void BOARD_InitPins(void)
{
    port_pin_config_t config = {0};

    // Ungate the used port clocks
    CLOCK_EnableClock(kCLOCK_PortA);
    CLOCK_EnableClock(kCLOCK_PortB);
    CLOCK_EnableClock(kCLOCK_PortC);
    CLOCK_EnableClock(kCLOCK_PortE);

    // Initialize UART1 pins below
    PORT_SetPinMux(PORTB, 16u, kPORT_MuxAlt3);
    PORT_SetPinMux(PORTB, 17u, kPORT_MuxAlt3);

    // Initialize SD pins below
    config.pullSelect = kPORT_PullUp;
    config.driveStrength = kPORT_HighDriveStrength;
    config.mux = kPORT_MuxAlt4;
    // SD component d1
    PORT_SetPinConfig(PORTE, 0u, &config);
    // SD component d0
    PORT_SetPinConfig(PORTE, 1u, &config);
    // SD component clk
    PORT_SetPinConfig(PORTE, 2u, &config);
    // SD component cmd
    PORT_SetPinConfig(PORTE, 3u, &config);
    // SD component d3
    PORT_SetPinConfig(PORTE, 4u, &config);
    // SD component d2
    PORT_SetPinConfig(PORTE, 5u, &config);

    // SD component card detection pin
    config.pullSelect = kPORT_PullDown;
    config.driveStrength = kPORT_LowDriveStrength;
    config.mux = kPORT_MuxAsGpio;

    PORT_SetPinConfig(PORTE, 6u, &config);

    // Configure RED LED
    PORT_SetPinMux(PORTB, 22u, kPORT_MuxAsGpio);

    // Configure PWM Signal from FTM0 CH0 and CH1
    PORT_SetPinMux(PORTC, 1u, kPORT_MuxAlt4);
    PORT_SetPinMux(PORTC, 2u, kPORT_MuxAlt4);

    // SW2 and SW3. May be later can do next/previous song
    PORT_SetPinMux(PORTA, 4u, kPORT_MuxAsGpio);
    PORT_SetPinMux(PORTC, 6u, kPORT_MuxAsGpio);
}

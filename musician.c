/*
 * musician.c
 *
 *  Created on: 5 Aug 2016
 *      Author: User
 */

#include "musician.h"

result musician_init()
{
	ftm_config_t ftmConfig;
	ftm_chnl_pwm_signal_param_t ftmParam[2];

	// Configure ftm parameters with frequency 1MHz (apparantly thats the maximum the k64f can do)
	ftmParam[0].chnlNumber = kFTM_Chnl_0;
	ftmParam[0].level = kFTM_LowTrue;
	ftmParam[0].dutyCyclePercent = 0u;
	ftmParam[0].firstEdgeDelayPercent = 0u;

	ftmParam[1].chnlNumber = kFTM_Chnl_1;
	ftmParam[1].level = kFTM_LowTrue;
	ftmParam[1].dutyCyclePercent = 0u;
	ftmParam[1].firstEdgeDelayPercent = 0u;

	FTM_GetDefaultConfig(&ftmConfig);
	FTM_Init(FTM0, &ftmConfig);

	FTM_SetupPwm(FTM0, ftmParam, 2u, kFTM_EdgeAlignedPwm, 1000000u, CLOCK_GetFreq(kCLOCK_BusClk));
	FTM_StartTimer(FTM0, kFTM_SystemClock);

	return Ok;
}

result musician_play(uint16_t right, uint16_t left)
{

	// Use bare metal code here because the FTM_UpdatePwmDutyCycle function
	// takes in a int % of the duty cycle. We are effectively losing the 8
	// bit quality of the recording, against a signal of 6 bit quality.
	// Now the signal is 16 bit resolution which is the maximum we might and
	// the maximum that the k64f supports;

	uint16_t newValue = 0,
			 maxValue = FTM0->MOD;

	// ---------------
	// 	LEFT
	// ---------------
	newValue = (maxValue * left) / 0xFFFF;
	if (newValue >= maxValue)
		newValue = maxValue + 1;
	FTM0->CONTROLS[kFTM_Chnl_0].CnV = newValue;

	// ---------------
	// 	RITE
	// ---------------
	newValue = (maxValue * right) / 0xFFFF;
	if (newValue >= maxValue)
		newValue = maxValue + 1;
	FTM0->CONTROLS[kFTM_Chnl_1].CnV = newValue;

	// FTM_UpdatePwmDutycycle(FTM0, kFTM_Chnl_0, kFTM_EdgeAlignedPwm, left);
	// FTM_UpdatePwmDutycycle(FTM0, kFTM_Chnl_1, kFTM_EdgeAlignedPwm, right);

	// Software trigger to update registers
	FTM_SetSoftwareTrigger(FTM0, true);

	return Ok;
}


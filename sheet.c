/*
 * sheet.c
 *
 *  Created on: 3 Aug 2016
 *      Author: User
 */

#include "sheet.h"

FATFS FatFs;
FIL score;

static uint32_t sheet[BUFFER_SIZE] = { 0 };
static volatile unsigned int read = 0;
static volatile unsigned int write = 0;

result sheet_init() {

    const TCHAR driverNumberBuffer[3U] = {SDDISK + '0', ':', '/'};
	FRESULT error;

	error = f_mount(&FatFs, driverNumberBuffer, 0);
	if(error)
		return RuntimeError;

    error = f_chdrive((char const *)&driverNumberBuffer[0U]);
	if(error)
		return RuntimeError;

	return Ok;
}

result sheet_start()
{
	printf("Starting\r\n");
	for (int i = 0; i < BUFFER_SIZE; i++)
		sheet[i] = 0;

	read = 0;
	write = 0;

	return Ok;
}

// Crudely inspired from sd8psrc http://elm-chan.org/works/sd8p/report.html
result sheet_sightSee(char *name, format *music) {

	FRESULT error;
	uint8_t buffer[BUFFER_SIZE] = { 0 };
    UINT bytesRead = 0;
    int size = 0;

	if(!(GPIO_ReadPinInput(BOARD_SDHC_CD_GPIO_BASE, BOARD_SDHC_CD_GPIO_PIN)))
	    return ExternalError;

	error = f_open(&score, name, FA_READ);
	if(error)
		return InvalidArgument;

	error = f_read(&score, buffer, 12, &bytesRead);
	if(error || bytesRead != 12)
		return RuntimeError;

	// read succeed ?
	// Load big endian word. Is it equal to 'W' 'A' 'V' 'E' ?
	if(parse4ByteBigEndian(buffer + 8) != 0x45564157)
		return InvalidProcessedData;

	for (;;) {

		// Read 8 bytes
		error = f_read(&score, buffer, 8, &bytesRead);
		if(error || bytesRead != 8)
			return RuntimeError;

		size = parse4ByteBigEndian(buffer + 4);

		switch(parse4ByteBigEndian(buffer + 0)) {
		case 0x20746d66: // 'f' 'm' 't'

			// Get Audio file information and do sanity check
			// Size here corresponds to the size of the rest
			// of the sub chunk
			if(size < 16 || size > 100)
				return InvalidProcessedData;

			error = f_read(&score, buffer, size, &bytesRead);
			if(error || bytesRead != size)
				return RuntimeError;

			// Audio format. Should be == 1 otherwise, this indicates
			// some form of compressions
			if(buffer[0] != 1)
				return InvalidProcessedData;

			// Number of channels.
			// 1 indicates mono, 2 means stereo. 3 etc... shows errors
			if(buffer[2] != 1 && buffer[2] != 2)
				return InvalidProcessedData;

			// Sample rate.
			if(parse4ByteBigEndian(buffer + 4) < 8000 || parse4ByteBigEndian(buffer + 4) > 48000)
				return InvalidProcessedData;

			// BitsPerSamples
			// Suport for oni 8 bits and 16 bits
			if(buffer[14] != 8 && buffer[14] != 16)
				return InvalidProcessedData;

			if(parse4ByteBigEndian(buffer + 8) != parse4ByteBigEndian(buffer + 4) * buffer[2] * buffer[14] / 8)
				return InvalidProcessedData;

			// Finally save everything!
			music->numChannels = parse2ByteBigEndian(buffer + 2);
			music->sampleRate = parse4ByteBigEndian(buffer + 4);
			music->bitsPerSample = parse2ByteBigEndian(buffer + 14);

			break;
		case 0x61746164: // 'd' 'a' 't' 'a'
			// Get size of the audio data

			music->size = size;
			return Ok;
		case 0x50534944: // 'D' 'I' 'S' 'P'
		case 0x5453494C: // 'L' 'I' 'S' 'T'
		case 0x74636166: // 'f' 'a' 'c' 't'
			if(size & 1) size++;
			f_lseek(&score, f_tell(&score) + size);
			break;
		default:
			return InvalidProcessedData;
		}
	}

	return RuntimeError;
}

bool sheet_end()
{
	return f_eof(&score);
}

result sheet_skim(format *music)
{

	uint8_t buffer[4] = { 0 };
	UINT bytesRead = 0;

	uint16_t numChannels = music->numChannels;
	uint16_t bytesPerSample = music->bitsPerSample / 8;

	if(!(GPIO_ReadPinInput(BOARD_SDHC_CD_GPIO_BASE, BOARD_SDHC_CD_GPIO_PIN)))
	    return ExternalError;


	// Will only get fetch from sd card if buffer is not full
	if(write - read != BUFFER_SIZE) {

		f_read(&score, &buffer, numChannels * bytesPerSample, &bytesRead);
		if(bytesRead != numChannels * bytesPerSample)
			return RuntimeError;

		// Apparantly 8 bit is recorded as unsigned little ended amplitude
		// However, 16 bit is recorded as signed little ended amplitudes
		// Because we are saving values in 16bit format, need to convert
		// the 8bit data to 16 by x256;
		// 16 bit data is a bit more complicated. Need to offset everyvalues
		// by 32760
		if(numChannels == 2 && bytesPerSample == 2)
			sheet[write & (BUFFER_SIZE - 1)] = (((((uint32_t) buffer[3] << 8) | (uint32_t) buffer[2]) + 32760) << 16) | ((((uint32_t) buffer[1] << 8) | (uint32_t) buffer[0]) + 32760);
		else if(numChannels == 1 && bytesPerSample == 2)
			sheet[write & (BUFFER_SIZE - 1)] = ((((uint32_t) buffer[1] << 8) | (uint32_t) buffer[0]) + 32760) | ((((uint32_t) buffer[1] << 8) | (uint32_t) buffer[0]) + 32760);
		else if(numChannels == 2 && bytesPerSample == 1)
			sheet[write & (BUFFER_SIZE - 1)] = (((uint32_t) 0x00 << 24) | ((uint32_t) buffer[1] << 16)) * 255 | (((uint32_t) 0x00 << 8) | ((uint32_t) buffer[0])) * 255;
		else if(numChannels == 1 && bytesPerSample == 1)
			sheet[write & (BUFFER_SIZE - 1)] = (((uint32_t) 0x00 << 8) | ((uint32_t) buffer[0])) * 255 | (((uint32_t) 0x00 << 8) | ((uint32_t) buffer[0])) * 255;
		else
			return InvalidArgument;

		write++;

	}

	return Ok;

}

uint32_t sheet_read()
{
	uint32_t ret = 0;

	// Read from the ring buffer oni if buffer is not empty
	if(write - read != 0) {

		ret = sheet[read & (BUFFER_SIZE - 1)];
		read++;

	}

	return ret;
}

result sheet_finish()
{

	f_close(&score);

	return Ok;

}


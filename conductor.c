/*
 * conductor.c
 *
 *  Created on: 4 Aug 2016
 *      Author: User
 */

#include "conductor.h"

volatile bool next_tempo = false;

void conductor_init()
{
	// Initialise the sheet music
	sheet_init();

	// Tell musicians 2 b ready
	musician_init();

	// Use the Periodic Interrupt Timer to produce
	// a periodic interrrupt lel
	pit_config_t pitConfig;

	LED_RED_INIT(LOGIC_LED_ON);

	PIT_GetDefaultConfig(&pitConfig);
	PIT_Init(PIT, &pitConfig);
}

void PIT0_IRQHandler(void)
{
    // Clear interrupt flag.
    PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, PIT_TFLG_TIF_MASK);

    next_tempo = true;

    LED_RED_TOGGLE();
}

void conductor_play(char *name)
{

	format music;
    uint32_t notes = 0;

	if(sheet_sightSee(name, &music) != Ok) {
		printf("Invalid file format\r\n");
		return;
	}
	printf("Music %s: %dbit %dch %dHz: %dsamples\r\n", name, music.bitsPerSample, music.numChannels, music.sampleRate, music.size);

	// Set the tempo xD
	PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT((int) (1000000/music.sampleRate), CLOCK_GetFreq(kCLOCK_BusClk)));
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
	EnableIRQ(PIT0_IRQn);
    PIT_StartTimer(PIT, kPIT_Chnl_0);

    sheet_start();

    while(!sheet_end()) {

        if(sheet_skim(&music) != Ok)
        	break;

        if(next_tempo) {
        	notes = sheet_read();

        	musician_play(notes >> 16, notes & 0xFFFF);
            next_tempo = false;
        }
    }

    sheet_finish();

	musician_play(0, 0);
	// Stop the tempo haha
    PIT_StopTimer(PIT, kPIT_Chnl_0);

    printf("Done playing\r\n");


}

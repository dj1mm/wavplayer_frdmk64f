/*
 * Copyright (c) 2013 - 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * About:
 * Wav music player. Uses Timer Interrupt, PWM and an SD card on the k64f board.
 * Supports 8bit and 16bit audio, 1 Stereo and Mono and plays wav from 8kHz to 48kHz
 * sampling rate.
 *
 * KSDKv2. KDSv3.
 *
 * Todo:
 * Although quoted 16bit PWM, the output is quite noisy. Sometimes, the output is distorted.
 * Work on this.
 * Do a NEXT/PREVIOUS PAUSE/PLAY button interface.
 *
 */

#include "main.h"

int main(void) {

	// Init everything
	BOARD_InitPins();
	BOARD_BootClockRUN();

	BOARD_InitDebugConsole();

	MPU_Enable(MPU, false);

	conductor_init();

	conductor_play("music001.wav");

	printf("DONE!\r\n");

	for(;;) { // Infinite loop to avoid leaving the main function
		__asm("NOP"); // something to use as a breakpoint stop while looping
	}
}
